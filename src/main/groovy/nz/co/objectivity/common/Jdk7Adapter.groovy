package nz.co.objectivity.common

import groovy.transform.CompileStatic
import org.codehaus.groovy.runtime.DefaultGroovyMethods

/**
 * @author Richard Vowles - https://google.com/+RichardVowles
 */
@CompileStatic
class Jdk7Adapter {
	public static <T> List<T> sort(List<T> self, Comparator<T> comparator) {
		if (!self) return self
		return DefaultGroovyMethods.sort((Iterable<T>)self, true)
	}
}

